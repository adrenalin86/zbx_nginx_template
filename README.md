#zbx_nginx_template

Zabbix template for Nginx (python)

It's accumulate nginx stats and parse the access.log (just pice of log at once) and push result in Zabbix through trap-messages

##System requirements

- [python](http://www.python.org/downloads/)
- [nginx](http://nginx.org/) with configured http_stub_status_module and access.log

## What's logging:

- Request\sec
- Response codes (200,301,302,403,404,500,503)\min
- Active\Keepalive connections
- Header and body reading
- Accepted, handled connections

## Install

1) Put `zbx_nginx_stats.py` into your scripts path (like: `/etc/zabbix/script/nginx/`) on your Zabbix agent hosts.

2) Change to your configuration in `zbx_nginx_stats_conf.py.example` and copy the file in `/etc/zabbix/zbx_nginx_stats_conf.py`.

3) In script path (`/etc/zabbix/script/nginx/`) do:
```
chmod +x zbx_nginx_stats.py
```

4) Configure cron to run script every one minute:
```
$ sudo crontab -e

*/1 * * * * /etc/zabbix/script/nginx/zbx_nginx_stats.py
```

or just copy `./zbx_nginx_stats_job` to `/etc/cron.d/`.

5) Import `zbx_nginx_template.xml` into zabbix in Tepmplate section web gui.

6) Add the following configurations to you Nginx configuration file.
```
location /nginx_stat {
  stub_status on;       # Turn on nginx stats
  access_log   off;     # We do not need logs for stats
  allow 127.0.0.1;      # Security: Only allow access from IP
  deny all;             # Deny requests from the other of the world
}
```

That is all :)
